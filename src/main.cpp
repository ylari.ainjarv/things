#include <TheThingsNetwork.h>
#include <CayenneLPP.h>
#include <SparkFunBME280.h>

const char *appEui = "70B3D57ED0013089"; // AppEUI and AppKey
const char *appKey = "08DFEA77C28222ACB93E13CE0ECA4A3D";

#define lora Serial1
#define debug Serial

#define freq TTN_FP_EU868 // TTN_FP_EU868 or TTN_FP_US915

TheThingsNetwork ttn(lora, debug, freq);

CayenneLPP lpp(51);

BME280 bme;

void setup() {
  lora.begin(57600);
  debug.begin(9600);
  while (!debug && millis() < 10000); // wait a maximum of 10s for Serial Monitor
  debug.println("-- STATUS");
  ttn.showStatus();
  debug.println("-- JOIN");
  ttn.join(appEui, appKey);
  bme.settings.commInterface = I2C_MODE;
  bme.settings.I2CAddress = 0x76;
  bme.settings.runMode = 3; //  3, Normal mode
  bme.settings.tStandby = 0; //  0, 0.5ms
  bme.settings.filter = 0; //  0, filter off
  // tempOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  bme.settings.tempOverSample = 1;
  // pressOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  bme.settings.pressOverSample = 1;
  // humidOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  bme.settings.humidOverSample = 1;
  // Start the sensor
  bme.begin();
}

void loop() {
  debug.println("-- LOOP");
  lpp.reset();
  float temperature = bme.readTempC();
  String strt = "Temperature: ";
  strt += temperature;
  strt += " °C";
  debug.println(strt);
  lpp.addTemperature(1, temperature);
  float humidity = bme.readFloatHumidity();
  String strh = "Humidity: ";
  strh += humidity;
  strh += " %";
  debug.println(strh);
  lpp.addRelativeHumidity(2, humidity);
  float pressure = bme.readFloatPressure();
  pressure = pressure / 100; // convert to hPa
  String strp = "Pressure: ";
  strp += pressure;
  strp += " hPa";
  debug.println(strp);
  lpp.addBarometricPressure(3, pressure);
  lpp.addGPS(4, 52.37365, 4.88650, 2);
  ttn.sendBytes(lpp.getBuffer(), lpp.getSize()); // send it off
  delay(10000);
}
